﻿#include "pch.h"
#include "framework.h"
#include "MultiThread.h"
#include "MultiThreadDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


DWORD WINAPI ThreadProc(LPVOID lpParam)
{
    ThreadInfo* Info = (ThreadInfo*)lpParam;
    CDC* dc = CWnd::FromHandle(Info->hWnd)->GetDC();
    for (int i = 0; i < 280; i++)
    {
        for (int j = Info->nOffset; j < Info->nOffset + 160; j++)
        {
            dc->SetPixel(j, 280 - i, Info->clrRGB);
        }
    }
    DeleteObject(dc);

    return 0;
}

// CMultipleThreadProDlg 对话框

CMultiThreadDlg::CMultiThreadDlg(CWnd* pParent /*=nullptr*/)
    : CDialogEx(IDD_MULTITHREAD_DIALOG, pParent)
{
    m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CMultiThreadDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CMultiThreadDlg, CDialogEx)
    ON_WM_PAINT()
    ON_WM_QUERYDRAGICON()
    ON_BN_CLICKED(IDC_BUTTON_S, &CMultiThreadDlg::OnBnClickedButtonS)
    ON_BN_CLICKED(IDC_BUTTON_M, &CMultiThreadDlg::OnBnClickedButtonM)
    ON_BN_CLICKED(IDC_BUTTON_3, &CMultiThreadDlg::OnBnClickedButton3)
END_MESSAGE_MAP()


// CMultipleThreadProDlg 消息处理程序

BOOL CMultiThreadDlg::OnInitDialog()
{
    CDialogEx::OnInitDialog();

    // 设置此对话框的图标。  当应用程序主窗口不是对话框时，框架将自动
    //  执行此操作
    SetIcon(m_hIcon, TRUE);			// 设置大图标
    SetIcon(m_hIcon, FALSE);		// 设置小图标

    // TODO: 在此添加额外的初始化代码

    return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。  对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CMultiThreadDlg::OnPaint()
{
    if (IsIconic())
    {
        CPaintDC dc(this); // 用于绘制的设备上下文

        SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

        // 使图标在工作区矩形中居中
        int cxIcon = GetSystemMetrics(SM_CXICON);
        int cyIcon = GetSystemMetrics(SM_CYICON);
        CRect rect;
        GetClientRect(&rect);
        int x = (rect.Width() - cxIcon + 1) / 2;
        int y = (rect.Height() - cyIcon + 1) / 2;

        // 绘制图标
        dc.DrawIcon(x, y, m_hIcon);
    }
    else
    {
        CDialogEx::OnPaint();
    }
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CMultiThreadDlg::OnQueryDragIcon()
{
    return static_cast<HCURSOR>(m_hIcon);
}

void CMultiThreadDlg::OnBnClickedButtonS()
{
    // TODO: 在此添加控件通知处理程序代码
    DWORD startTime = GetTickCount();//计时开始

    GetDlgItem(IDC_BUTTON_S)->EnableWindow(FALSE);
    GetDlgItem(IDC_BUTTON_M)->EnableWindow(FALSE);
    CDC* dc = GetDC();
    CRect rt;
    GetClientRect(rt);
    dc->FillSolidRect(0, 0, rt.Width(), rt.Height() - 70, RGB(240, 240, 240));//刷新背景
    dc->TextOut(97, 290, TEXT("#1"));
    dc->TextOut(277, 290, TEXT("#2"));
    dc->TextOut(457, 290, TEXT("#3"));
    //#1
    for (int i = 0; i < 280; i++)
    {
        for (int j = 10; j < 170; j++)
        {
            dc->SetPixel(j, 280 - i, RGB(255, 0, 0));
        }
    }
    //#2
    for (int i = 0; i < 280; i++)
    {
        for (int j = 190; j < 350; j++)
        {
            dc->SetPixel(j, 280 - i, RGB(0, 255, 0));
        }
    }
    //#3
    for (int i = 0; i < 280; i++)
    {
        for (int j = 370; j < 530; j++)
        {
            dc->SetPixel(j, 280 - i, RGB(0, 0, 255));
        }
    }
    ReleaseDC(dc);

    //使能按钮
    GetDlgItem(IDC_BUTTON_S)->EnableWindow(TRUE);
    GetDlgItem(IDC_BUTTON_M)->EnableWindow(TRUE);

    DWORD endTime = GetTickCount();//计时结束
    CString sTime;
    sTime.Format(_T("The run time is: %d 毫秒"), endTime - startTime);
    MessageBox(sTime);
}

//多线程测试
void CMultiThreadDlg::OnBnClickedButtonM()
{
    // TODO: 在此添加控件通知处理程序代码
    //DWORD startTime = GetTickCount();//计时开始

    GetDlgItem(IDC_BUTTON_S)->EnableWindow(FALSE);
    GetDlgItem(IDC_BUTTON_M)->EnableWindow(FALSE);

    CDC* dc = GetDC();
    CRect rt;
    GetClientRect(rt);
    dc->FillSolidRect(0, 0, rt.Width(), rt.Height() - 70, RGB(240, 240, 240));//刷新背景
    dc->TextOut(97, 290, TEXT("#1"));
    dc->TextOut(277, 290, TEXT("#2"));
    dc->TextOut(457, 290, TEXT("#3"));

    //初始化线程的参数
    Info[0].hWnd = Info[1].hWnd = Info[2].hWnd = GetSafeHwnd();
    Info[0].nOffset = 10; Info[1].nOffset = 190; Info[2].nOffset = 370;
    Info[0].clrRGB = RGB(255, 0, 0); Info[1].clrRGB = RGB(0, 255, 0); Info[2].clrRGB = RGB(0, 0, 255);
    //创建线程
    for (int i = 0; i < 3; i++)
    {
        hThead[i] = CreateThread(NULL, 0, ThreadProc, &Info[i], 0, &dwThreadID[i]);
    }
    ReleaseDC(dc);

    //DWORD endTime = GetTickCount();//计时结束
    //CString sTime;
    //sTime.Format(_T("The run time is: %d 毫秒"), endTime - startTime);
    //MessageBox(sTime);

    //使能按钮
    GetDlgItem(IDC_BUTTON_S)->EnableWindow(TRUE);
    GetDlgItem(IDC_BUTTON_M)->EnableWindow(TRUE);
}


void CMultiThreadDlg::OnBnClickedButton3()
{
    // TODO: 在此添加控件通知处理程序代码
    CRect rectDlg;
    GetClientRect(rectDlg); // 得到客户区的位置和大小
    //GetWindowRect(rectDlg);//获得窗体在屏幕上的位置大小
    ScreenToClient(rectDlg);

    CRect rectS, rectM;
    GetDlgItem(IDC_BUTTON_S)->GetWindowRect(&rectS);//获取控件相对于屏幕的位置
    ScreenToClient(rectS);//转化为对话框上的相对位置
    GetDlgItem(IDC_BUTTON_M)->GetWindowRect(&rectM);//获取控件相对于屏幕的位置
    ScreenToClient(rectM);//转化为对话框上的相对位置
}


