﻿#pragma once

//线程函数声明
DWORD WINAPI ThreadProc(LPVOID lpParam);
//为了传递多个参数，我采用结构体
struct ThreadInfo
{
	HWND hWnd;       //窗口句柄
	int  nOffset;    //偏移量
	COLORREF clrRGB; //颜色
};


// CMultipleThreadProDlg 对话框
class CMultiThreadDlg : public CDialogEx
{
	// 构造
public:
	CMultiThreadDlg(CWnd* pParent = nullptr);	// 标准构造函数

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_MULTITHREAD_DIALOG };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持


// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

protected:
	HANDLE hThead[3];    //用于存储线程句柄
	DWORD  dwThreadID[3];//用于存储线程的ID
	ThreadInfo Info[3];   //传递给线程处理函数的参数

public:
	afx_msg void OnBnClickedButtonS();
	afx_msg void OnBnClickedButtonM();
	afx_msg void OnBnClickedButton3();
};
